import { Observable, Subject } from "rxjs";

interface ITimerParams {
  /** default = 60 */
  initial_value?: number;
  /** default = false */
  pause_on_document_hidden?: boolean;
  /** events for detect activity (for window.addEventListener) */
  additional_event_types?: string[];
}

const DEFAULT_EVENT_TYPES = ['mousemove', 'keypress', 'mousedown', 'mousewheel'];

export class InactivityTimer {
  public get timeout$(): Observable<void> {
    return this._timeout$.asObservable();
  }

  private default_value: number = 60;
  private seconds: number = 0;
  private interval: any;
  private readonly _timeout$: Subject<void> = new Subject();
  private is_destroyed: boolean = false;

  // @ts-ignore
  private event_listener = function() {this.seconds = this.default_value}.bind(this);

  constructor(private params?: ITimerParams) {
    if (params) {
      if (params.initial_value) {
        this.default_value = params.initial_value;
      }
      if (params.additional_event_types?.length) {
        params.additional_event_types.forEach((ev) => {
          window.addEventListener(ev, this.event_listener);
        })
      }
    }
    DEFAULT_EVENT_TYPES.forEach((ev) => {
      window.addEventListener(ev, this.event_listener);
    })
  }

  public setSeconds(seconds: number) {
    this.checkDestroyed();
    this.default_value = seconds;
    this.pause();
    this.resetTimer();
  }

  public start() {
    this.pause();
    this.resetTimer();
    this.resume();
  }

  public pause() {
    this.checkDestroyed();
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  public resume() {
    this.checkDestroyed();
    if (!this.interval) {
      this.interval = setInterval(() => {
        if (!this.params?.pause_on_document_hidden || !document.hidden) {
          this.seconds--;
        }
        if (this.seconds <= 0) {
          this.seconds = 0;
          this.pause();
          this._timeout$.next();
        }
      }, 1000);
    }
  }

  public resetTimer() {
    this.checkDestroyed();
    this.seconds = this.default_value;
  }

  public destroy() {
    if (!this.is_destroyed) {
      this.is_destroyed = true;
      if (this.params?.additional_event_types?.length) {
        this.params.additional_event_types.forEach((ev) => {
          window.removeEventListener(ev, this.event_listener);
        })
      }
      DEFAULT_EVENT_TYPES.forEach((ev) => {
        window.removeEventListener(ev, this.event_listener);
      })
    }
  }

  private checkDestroyed() {
    if (this.is_destroyed) {
      throw Error('InactivityTimer destroyed');
    }
  }
}
