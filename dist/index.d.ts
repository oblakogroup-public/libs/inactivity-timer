import { Observable } from "rxjs";
interface ITimerParams {
    /** default = 60 */
    initial_value?: number;
    /** default = false */
    pause_on_document_hidden?: boolean;
    /** events for detect activity (for window.addEventListener) */
    additional_event_types?: string[];
}
export declare class InactivityTimer {
    private params?;
    get timeout$(): Observable<void>;
    private default_value;
    private seconds;
    private interval;
    private readonly _timeout$;
    private is_destroyed;
    private event_listener;
    constructor(params?: ITimerParams | undefined);
    setSeconds(seconds: number): void;
    start(): void;
    pause(): void;
    resume(): void;
    resetTimer(): void;
    destroy(): void;
    private checkDestroyed;
}
export {};
