"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InactivityTimer = void 0;
var rxjs_1 = require("rxjs");
var DEFAULT_EVENT_TYPES = ['mousemove', 'keypress', 'mousedown', 'mousewheel'];
var InactivityTimer = /** @class */ (function () {
    function InactivityTimer(params) {
        var _this = this;
        var _a;
        this.params = params;
        this.default_value = 60;
        this.seconds = 0;
        this._timeout$ = new rxjs_1.Subject();
        this.is_destroyed = false;
        // @ts-ignore
        this.event_listener = function () { this.seconds = this.default_value; }.bind(this);
        if (params) {
            if (params.initial_value) {
                this.default_value = params.initial_value;
            }
            if ((_a = params.additional_event_types) === null || _a === void 0 ? void 0 : _a.length) {
                params.additional_event_types.forEach(function (ev) {
                    window.addEventListener(ev, _this.event_listener);
                });
            }
        }
        DEFAULT_EVENT_TYPES.forEach(function (ev) {
            window.addEventListener(ev, _this.event_listener);
        });
    }
    Object.defineProperty(InactivityTimer.prototype, "timeout$", {
        get: function () {
            return this._timeout$.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    InactivityTimer.prototype.setSeconds = function (seconds) {
        this.checkDestroyed();
        this.default_value = seconds;
        this.pause();
        this.resetTimer();
    };
    InactivityTimer.prototype.start = function () {
        this.pause();
        this.resetTimer();
        this.resume();
    };
    InactivityTimer.prototype.pause = function () {
        this.checkDestroyed();
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    };
    InactivityTimer.prototype.resume = function () {
        var _this = this;
        this.checkDestroyed();
        if (!this.interval) {
            this.interval = setInterval(function () {
                var _a;
                if (!((_a = _this.params) === null || _a === void 0 ? void 0 : _a.pause_on_document_hidden) || !document.hidden) {
                    _this.seconds--;
                }
                if (_this.seconds <= 0) {
                    _this.seconds = 0;
                    _this.pause();
                    _this._timeout$.next();
                }
            }, 1000);
        }
    };
    InactivityTimer.prototype.resetTimer = function () {
        this.checkDestroyed();
        this.seconds = this.default_value;
    };
    InactivityTimer.prototype.destroy = function () {
        var _this = this;
        var _a, _b;
        if (!this.is_destroyed) {
            this.is_destroyed = true;
            if ((_b = (_a = this.params) === null || _a === void 0 ? void 0 : _a.additional_event_types) === null || _b === void 0 ? void 0 : _b.length) {
                this.params.additional_event_types.forEach(function (ev) {
                    window.removeEventListener(ev, _this.event_listener);
                });
            }
            DEFAULT_EVENT_TYPES.forEach(function (ev) {
                window.removeEventListener(ev, _this.event_listener);
            });
        }
    };
    InactivityTimer.prototype.checkDestroyed = function () {
        if (this.is_destroyed) {
            throw Error('InactivityTimer destroyed');
        }
    };
    return InactivityTimer;
}());
exports.InactivityTimer = InactivityTimer;
//# sourceMappingURL=index.js.map